from sms.services.logics import post, get, get_std, get_all, Events_post, Events_get, event_all, studentget_all, \
    Student_post, Library_post, Library_get, update, Libraryget_all, ads, award_get, award_all, Financial_post, \
    Financial_get, Financial_all
from flask import Blueprint

bp = Blueprint("bp", __name__)


@bp.route('/post', methods=['POST'])
def post_fun():
    return post()


@bp.route('/get_all', methods=['GET'])
def get_all_fun():
    return get_all()


@bp.route('/get_atr', methods=['GET'])
def get_fun():
    return get()


@bp.route('/Events_post', methods=['POST'])
def Events_post_fun():
    return Events_post()


@bp.route('/Events_get', methods=['GET'])
def Events_get_fun():
    return Events_get()


@bp.route('/event_all', methods=['GET'])
def event_all_fun():
    return event_all()


@bp.route('/Student_post', methods=['POST'])
def Student_post_fun():
    return Student_post()


@bp.route('/get_std', methods=['GET'])
def get_std_fun():
    return get_std()


@bp.route('/studentget_all', methods=['GET'])
def studentget_all_fun():
    return studentget_all()


@bp.route('/Library_post', methods=['POST'])
def Library_post_fun():
    return Library_post()


@bp.route('/Library_std', methods=['GET'])
def Library_get_fun():
    return Library_get()


@bp.route('/update', methods=['PUT'])
def update_fun():
    return update()


@bp.route('/Libraryget_all', methods=['GET'])
def Libraryget_all_fun():
    return Libraryget_all()


@bp.route('/ads', methods=['POST'])
def ads_fun():
    return ads()


@bp.route('/award', methods=['GET'])
def award_get_fun():
    return award_get()


@bp.route('/award_all', methods=['GET'])
def award_all_fun():
    return award_all()


@bp.route('/Financial_post', methods=['POST'])
def Financial_post_fun():
    return Financial_post()


@bp.route('/Financial_get', methods=['GET'])
def Financial_get_fun():
    return Financial_get()


@bp.route('/Financial_all', methods=['GET'])
def Financial_all_fun():
    return Financial_all()
