from sms.app import db


class Teacher_portal(db.Model):
    teacher_id = db.Column(db.Integer, primary_key=True)
    teacher_name = db.Column(db.String(80), unique=True, nullable=False)
    events = db.relationship('Events', backref='Teacher_portal')
    stud = db.relationship('Student_portal', backref='Teacher_portal')

class Events(db.Model):
    events_id = db.Column(db.Integer, primary_key=True)
    class_name = db.Column(db.String(120), nullable=False)
    drawing = db.Column(db.Date, nullable=False)
    painting = db.Column(db.Date, nullable=False)
    singing = db.Column(db.Date, nullable=False)
    dancing = db.Column(db.Date, nullable=False)
    events_place = db.Column(db.String(120), nullable=False)
    teacher_id = db.Column(db.Integer, db.ForeignKey('teacher_portal.teacher_id'), nullable=False)
    student_id = db.Column(db.Integer, db.ForeignKey('student_portal.student_id'), nullable=False)
    events = db.relationship('Awards', backref='Events')


class Student_portal(db.Model):
    student_id = db.Column(db.Integer, primary_key=True)
    student_name = db.Column(db.String(80), unique=True, nullable=False)
    student_class = db.Column(db.String(300), nullable=False)
    parent_name = db.Column(db.String(120), unique=True, nullable=False)
    teacher_id = db.Column(db.Integer, db.ForeignKey('teacher_portal.teacher_id'), nullable=False)
    events = db.relationship('Events', backref='Student_portal')
    lib = db.relationship('Library', backref='Student_portal')


class Library(db.Model):
    Library_id = db.Column(db.Integer, primary_key=True)
    book_name = db.Column(db.String(80), unique=True, nullable=False)
    dateofbook_taken = db.Column(db.Date, nullable=False)
    dateofbook_return = db.Column(db.Date, nullable=True)
    author_name = db.Column(db.String(120), unique=True, nullable=False)
    student_id = db.Column(db.Integer, db.ForeignKey('student_portal.student_id'), nullable=False)


class Awards(db.Model):
    award_id = db.Column(db.Integer, primary_key=True)
    awardees = db.Column(db.String(80), unique=True, nullable=False)
    award_category = db.Column(db.String(120), unique=True, nullable=False)
    events_id = db.Column(db.Integer, db.ForeignKey('events.events_id'), nullable=False)


class Financial_Department(db.Model):
    finance_id = db.Column(db.Integer, primary_key=True)
    class_fee_std = db.Column(db.String(80), unique=True, nullable=False)
    term1 = db.Column(db.String(80), unique=True, nullable=False)
    term2 = db.Column(db.String(80), unique=True, nullable=False)
    term3 = db.Column(db.String(80), unique=True, nullable=False)
    yearly = db.Column(db.String(80), unique=True, nullable=False)
    student_id = db.Column(db.Integer, db.ForeignKey('student_portal.student_id'), nullable=False)
