import os

from flask import request, jsonify
from functools import wraps
from sms.models.entities import Teacher_portal, Student_portal, Events, Financial_Department, Awards, Library, db
from sqlalchemy import exc


def authorization(fun):
    @wraps(fun)
    def inner_fun(*args, **kwargs):
        login_key = request.headers.get("Token")
        check_key = os.environ.get("token")
        if login_key == check_key:
            return fun(*args, **kwargs)
        else:
            return "admin_access_denied"

    return inner_fun


def post():
    try:
        req = request.json
        new_post = Teacher_portal.query.all()
        for value in new_post:
            if req.get('teacher_name') == value.teacher_name:
                return "teacher_name is already in use, Please enter another name:"
        var = Teacher_portal(**req)
        db.session.add(var)
        db.session.commit()
        return "Teacher_portal_added"
    except TypeError:
        return "Enter field name properly !!"


def get_all():
    teach = Teacher_portal.query.all()
    u = []
    for teachs in teach:
        teachs = teachs.__dict__
        teachs.pop("_sa_instance_state")
        u.append(teachs)
    return jsonify(u)


def get():
    req_get = request.json
    Teach = Teacher_portal.query.get(req_get.get("teacher_id"))
    Teachs = Teach.__dict__
    Teachs.pop("_sa_instance_state")
    return Teachs


def Events_post():
    try:
        req = request.json
        Events_post = Events(**req)
        db.session.add(Events_post)
        db.session.commit()
        return "Events_added"
    except exc.IntegrityError:
        return "Enter all the fields in Table"

def Events_get():
    req = db.session.query(
        Student_portal, Teacher_portal, Events).filter(
        Student_portal.student_id == Events.student_id).filter(
        Teacher_portal.teacher_id == Events.teacher_id).filter(
        Events.student_id == request.json.get("student_id"))
    for take in req:
        taken = take.Events
        val = taken.__dict__
        val['drawing'] = val['drawing'].isoformat()
        val['painting'] = val['painting'].isoformat()
        val['singing'] = val['singing'].isoformat()
        val['dancing'] = val['dancing'].isoformat()
        val.pop('_sa_instance_state')
    return jsonify(val)


def event_all():
    event = Events.query.all()
    u = []
    for val in event:
        val = val.__dict__
        val['drawing'] = val['drawing'].isoformat()
        val['painting'] = val['painting'].isoformat()
        val['singing'] = val['singing'].isoformat()
        val['dancing'] = val['dancing'].isoformat()
        val.pop("_sa_instance_state")
        u.append(val)
    return jsonify(u)


def Student_post():
    try:
        req = request.json
        new_post = Student_portal.query.all()
        for value in new_post:
            if req.get('student_name') == value.student_name:
                return "student_name is already in use, Please enter another name:"
        var = Student_portal(**req)
        db.session.add(var)
        db.session.commit()
        return "Student_portal_added"
    except TypeError:
        return "Enter field name properly !!"
    except exc.IntegrityError:
        return "Enter every field"


def get_std():
    req_get = request.json
    Stud = Student_portal.query.get(req_get.get("student_id"))
    Studs = Stud.__dict__
    Studs.pop("_sa_instance_state")
    return Studs


def studentget_all():
    stu_all = Student_portal.query.all()
    u = []
    for stud_all in stu_all:
        stud_all = stud_all.__dict__
        stud_all.pop("_sa_instance_state")
        u.append(stud_all)
    return jsonify(u)


def Library_post():
    try:
        req = request.json
        Library_post = Library(**req)
        db.session.add(Library_post)
        db.session.commit()
        return "Library_details_added"
    except exc.IntegrityError:
        return "Enter every field"


def Library_get():
    x = db.session.query(
        Library, Student_portal).filter(
        Library.Library_id == Student_portal.student_id).filter(
        Library.student_id == request.json.get("student_id")).all()
    for take in x:
        taken = take.Library
        value = taken.__dict__
        value['dateofbook_taken'] = value['dateofbook_taken'].isoformat()
        value.pop('_sa_instance_state')
        return jsonify(value)
    return "done"


@authorization
def update():
    req = request.json
    Library_id = Library.query.get(req.get("Library_id"))
    Library_id.dateofbook_return = req.get("dateofbook_return")
    db.session.commit()
    return "update_date_of_book_return"


def Libraryget_all():
    Lib_all = Library.query.all()
    u = []
    for Libs_all in Lib_all:
        Libs_all = Libs_all.__dict__
        Libs_all.pop("_sa_instance_state")
        u.append(Libs_all)
    return jsonify(u)


def ads():
    try:
        req = request.json
        Awards_post = Awards(**req)
        db.session.add(Awards_post)
        db.session.commit()
        return "Awards_Detail_filled"
    except exc.IntegrityError:
        return "Enter every field"


def award_get():
    x = db.session.query(
        Awards, Events).filter(
        Awards.award_id == Events.events_id).filter(
        Awards.events_id == request.json.get("events_id")).all()
    for take in x:
        taken = take.Awards
        value = taken.__dict__
        value.pop('_sa_instance_state')
        return jsonify(value)


def award_all():
    Award_all = Awards.query.all()
    u = []
    for award_all in Award_all:
        award_all = award_all.__dict__
        award_all.pop("_sa_instance_state")
        u.append(award_all)
    return jsonify(u)


def Financial_post():
    # try:
    req = request.json
    Financial_post = Financial_Department(**req)
    db.session.add(Financial_post)
    db.session.commit()
    return "Financial_values_added"


# except exc.IntegrityError:
#     return "Enter every field"

def Financial_get():
    x = db.session.query(
        Financial_Department, Student_portal).filter(
        Financial_Department.finance_id == Student_portal.student_id).filter(
        Financial_Department.student_id == request.json.get("student_id")).all()
    for take in x:
        taken = take.Financial_Department
        value = taken.__dict__
        value.pop('_sa_instance_state')
        return jsonify(value)
        return "done"


def Financial_all():
    Financial_all = Financial_Department.query.all()
    u = []
    for finacial_all in Financial_all:
        finacial_all = finacial_all.__dict__
        finacial_all.pop("_sa_instance_state")
        u.append(finacial_all)
    return jsonify(u)
