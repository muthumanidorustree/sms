from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sms.config import DevelopmentConfig, ProductionConfig
import pymysql

pymysql.install_as_MySQLdb()

db = SQLAlchemy()


def app_create(config=DevelopmentConfig):
    app = Flask(__name__)
    app.config.from_object(config)
    db.init_app(app)

    with app.app_context():
        from sms.api import routes
        app.register_blueprint(routes.bp)
        db.create_all()

    return app
